<?php $__env->startSection('content'); ?>
   <h1><?php echo e($title); ?></h1>
   <?php if(count($contacts) > 0): ?>
      <ul class="list-group">
         <?php $__currentLoopData = $contacts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $contact => $contact_value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li class="list-group-item"><?php echo e($contact.': '.$contact_value); ?></li>
         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>     
      </ul>
   <?php endif; ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>