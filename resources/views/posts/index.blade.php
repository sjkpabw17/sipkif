<!-- DataTables -->
@extends('layouts.applogin')

@section('content-dash')
	<!-- Breadcrumbs-->
	<ol class="breadcrumb">
		<li class="breadcrumb-item">
		<a href="/dashboard">Dashboard</a>
		</li>
		<li class="breadcrumb-item active">Laporan</li>
	</ol>
	<div class="container">
		<h1 style="margin-bottom:3%">Laporan</h1>
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				<thead>
					<tr>
						<th>ID</th>
						<th>Jenis Infrastruktur</th>
						<th>Lokasi</th>
						<th>Tingkat Kerusakan</th>
						<th>Keterangan</th>
						<th colspan="2">Aksi</th>
					</tr>
				</thead>

				@if (count($posts) > 0)
				<tbody>
					@foreach ($posts as $post)
						<tr>
							<th>{{$post->id}}</th>
							<th><a href="/posts/{{$post->id}}">{{$post->jenis_infrastruktur}}</a></th>
							<td>{{$post->lokasi}}</td>
							<td>{{$post->tingkat_kerusakan}}</td>
							<td>{{$post->keterangan}}</td>
							<td>
								<a class="btn btn-primary" href="/posts/{{$post->id}}/edit">Edit</a>
							</td>
							<td>
								{!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
									{{Form::hidden('_method', 'DELETE')}}
									{{Form::submit('Hapus', ['class' => 'btn btn-danger'])}}
								{!!Form::close()!!}
							</td>
						</tr>
					@endforeach
				</tbody>
				@else
					<p>Laporan tidak ditemukan</p>
				@endif
			</table>
		</div>
	</div>
@endsection
