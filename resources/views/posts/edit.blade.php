@extends('layouts.applogin')

@section('content-dash')
<ol class="breadcrumb">
	<li class="breadcrumb-item">
		<a href="/dashboard">Dashboard</a>
	</li>
	<li class="breadcrumb-item active">Laporan</li>
	<li class="breadcrumb-item active">Edit</li>
</ol>

<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">
					<h3 class="text-center">Ubah Laporan</h3>
				</div>
				<div class="card-body">
					{!! Form::open(['action' => ['PostsController@update', $post->id], 'method' => 'POST', 'files' => true, 'enctype'=> 'multipart/form-data'] ) !!}
						{{-- <input type="hidden" name="_method" value="PATCH"> --}}
						<div class="form-group row">
							{{Form::label('jenis_infrastruktur', 'Jenis Infrastruktur', ['class' => 'col-sm-3'])}}
							<div class="col-md-6">
								{{Form::text('jenis_infrastruktur', $post->jenis_infrastruktur, ['class' => 'form-control', 'placeholder' => 'Jenis Infrastruktur'])}}
							</div>
						</div>
						<div class="form-group row">
							{{Form::label('upload', 'Bukti Kerusakan', ['class' => 'col-sm-3'])}}
							<div class="col-md-6">
								{{-- {{Form::file('upload', ['class' => 'form-control-file'])}} --}}
								{{-- <img src="{{url('images/'.$post->original)}}"> --}}
								<div id="gambar">
									<img src="{{url('images/'.$post->original)}}">
									<button type="button" onclick="hapus()">Ganti</button>
								</div>
								<div id="tambah">

								</div>

								<input type="hidden" value="{{$post->original}}" name="old">

							</div>
						</div>
						<div class="form-group row">
							{{Form::label('lokasi', 'Lokasi', ['class' => 'col-sm-3'])}}
							<div class="col-md-6">
								{{Form::text('lokasi', $post->lokasi, ['class' => 'form-control', 'placeholder' => 'Lokasi'])}}
							</div>
						</div>
						<div class="form-group row">
							{{Form::label('tingkat_kerusakan', 'Tingkat Kerusakan', ['class' => 'col-sm-3'])}}
							<div class="col-md-6">
								{{Form::select('tingkat_kerusakan', ['Ringan' => 'Ringan', 'Sedang' => 'Sedang', 'Berat' => 'Berat'], $post->tingkat_kerusakan, ['placeholder' => '--Tingkat Kerusakan--'])}}
							</div>
						</div>
						<div class="form-group row">
							{{Form::label('keterangan', 'Keterangan', ['class' => 'col-sm-3'])}}
							<div class="col-md-8">
								{{Form::textarea('keterangan', $post->keterangan, ['class' => 'form-control', 'placeholder' => 'Keterangan'])}}
							</div>
						</div>
						<div align=right>
							{{Form::hidden('_method', 'PUT')}}
							{{Form::submit('Ubah', ['class' => 'btn btn-primary'])}}
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
