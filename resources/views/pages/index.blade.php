@extends('layouts.app')

@section('content')
<header class="masthead text-center text-white d-flex">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 mx-auto">
            <h1 class="text-uppercase">
              <strong>SIPKIF</strong>
            </h1>
            <hr>
          </div>
          <div class="col-lg-8 mx-auto">
            <p class="text-faded mb-5">Sistem Informasi Pendataan Kerusakan Infrastruktur dan Fasilitas Publik</p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about">Ketahui Lebih Jauh</a>
          </div>
        </div>
      </div>
    </header>

    <section class="bg-primary" id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading text-white">Apa yang bisa anda lakukan dengan SIPKIF ?</h2>
            <hr class="light my-4">
            <p class="text-faded mb-4">Dengan SIPKIF anda dapat membantu mempermudah kinerja BPBD
              dalam mengelola data kerusakan infrastruktur dan fasilitas publik yang ada pada suatu wilayah
              dengan meminta peran langsung dari masyarakat yaitu dengan cara melaporkan suatu kerusakan
              infrastruktur dan fasilitas publik melalui SIPKIF ini.</p>
            <a class="btn btn-light btn-xl js-scroll-trigger" href="#services">AYO MULAI!</a>
          </div>
        </div>
      </div>
    </section>

    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading">Layanan</h2>
            <hr class="my-4">
          </div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <a href="#pilihLogin">
              <img src="img/icon/lapor.png">
              <h3 class="mb-3">Lapor</h3>
              </a>
              <p class="text-muted mb-0">Lapor kerusakan infrastruktur</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <a href="#pilihLogin">
              <img src="img/icon/cekLaporan.png">
              <h3 class="mb-3">Cek status Laporan</h3>
              </a>
              <p class="text-muted mb-0">Kamu bisa melihat status laporan</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <a href="#pilihLogin">
              <img src="img/icon/editProfile.png">
              <h3 class="mb-3">Ubah profil</h3>
              </a>
              <p class="text-muted mb-0">Ubah Profil Anda</p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="service-box mt-5 mx-auto">
              <a href="">
              <img src="img/icon/bpbd2.png">
              <h3 class="mb-3">Informasi</h3>
              </a>
              <p class="text-muted mb-0">Cari informasi tentang BPBD</p>
            </div>
          </div>
        </div>
      </div>

    </section>


    <section class="bg-dark text-white" id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-8 mx-auto text-center">
            <h2 class="section-heading">Kontak</h2>
            <hr class="my-4">
            <p class="mb-5">Anda memerlukan bantuan ? Hubungi kami atau kirim email kepada kami dan kami akan menghubungi Anda sesegera mungkin</p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-4 ml-auto text-center">
            <i class="fas fa-phone fa-3x mb-3 sr-contact-1"></i>
            <p>081-234-567-234</p>
          </div>
          <div class="col-lg-4 mr-auto text-center">
            <i class="fas fa-envelope fa-3x mb-3 sr-contact-2"></i>
            <p>
              <a href="mailto:your-email@your-domain.com">tayo.corp@gmail.com</a>
            </p>
          </div>
        </div>
      </div>
	 </section>
	 <footer class="footer">
			<p >&copy Tayo 2018</p>
		</footer>
@endsection
