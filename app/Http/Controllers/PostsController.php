<?php

namespace App\Http\Controllers;
// use App\Http\Controllers\File;
use File;

use Illuminate\Http\Request;
use App\Post;
use App\User;

class PostsController extends Controller
{
	// public function __construct()
	// {
	// 	$this->middleware('guest');
	// }
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		// $posts = Post::orderBy('created_at', 'desc')->paginate(6);
		$user_id = auth()->user()->id;
		$user = User::find($user_id);

		return view('posts.index')->with('posts', $user->posts);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('posts.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'jenis_infrastruktur' => 'required',
			// 'upload' => 'required|image|mimes:jpeg, png, jpg, mp4, gif, svg',
			'lokasi' => 'required',
			'tingkat_kerusakan' => 'required',
			'keterangan'
		]);
		$imageName = $request->file('upload');
		$extension = $imageName->getClientOriginalExtension();
		request()->upload->move(public_path('images'), $imageName->getClientOriginalName());

		//Create post
		$post = new Post;
		$post->jenis_infrastruktur = $request->input('jenis_infrastruktur');
		$post->mime = $imageName->getClientMimeType();
		$post->original = $imageName->getClientOriginalName();
		$post->lokasi = $request->input('lokasi');
		$post->tingkat_kerusakan = $request->input('tingkat_kerusakan');
		$post->keterangan = $request->input('keterangan');
		$post->status = $request->input('status');
		$post->user_id = auth()->user()->id;
		$post->save();
		return redirect('/dashboard')->with('success', 'Laporan Berhasil Dibuat');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		$post = Post::find($id);
		return view('posts.show')->with('post', $post);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		$post = Post::find($id);
		return view('posts.edit')->with('post', $post);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$post = Post::find($id);
		$this->validate($request, [
			'jenis_infrastruktur' => 'required',
			// 'upload' => 'image|mimes:jpeg, png, jpg, mp4, gif, svg',
			'lokasi' => 'required',
			'tingkat_kerusakan' => 'required',
			'keterangan'
		]);
		// request()->validate([
		// 	'upload' => 'image|mimes:jpeg, png, jpg, mp4, gif, svg',
		// ]);
		$imageName = $request->file('upload');
		if ($imageName != null) {
			$old = $request->get('old');
			File::delete('images/' . $old);
			$extension = $imageName->getClientOriginalExtension();
			request()->upload->move(public_path('images'), $imageName->getClientOriginalName());
			$post->mime = $imageName->getClientMimeType();
			$post->original = $imageName->getClientOriginalName();
		}

		//Update post
		$post->jenis_infrastruktur = $request->input('jenis_infrastruktur');
		$post->lokasi = $request->input('lokasi');
		$post->tingkat_kerusakan = $request->input('tingkat_kerusakan');
		$post->status = $request->input('status');
		$post->keterangan = $request->input('keterangan');
		$post->status = $request->input('status');
		$post->save();

		return redirect('/dashboard')->with('success', 'Laporan Berhasil Diubah');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		$post = Post::find($id);
		$post->delete();

		return redirect('/dashboard')->with('success', 'Laporan Berhasil Dihapus');
	}
}
